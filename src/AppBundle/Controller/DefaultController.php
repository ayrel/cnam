<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Photo;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $ad = new Ad();

        $ad->setDescription("MAISON DE VILLE Quartier Jean-Jaurès, 50m de la Place, au calme, fond de cour cette jolie maison de 125 m² sur 2 étages vous propose 4 chambres, 2 salles d'eau, 2 WC, un beau séjour de 25m², une grande cuisine, ces deux pièces au rez-de-chaussée avec accès au jardin de 80 m². 1 cave. Chauffage individuel au gaz, pas de travaux, en copropriété avec un petit immeuble formant 49 lots, la maison représente 351/3023 tantième des charges, Charges de 50€/mois soit 600€/an.Cette maison est actuellement louée en colocation meublée avec un bon rendement et peut être libre de tout occupant rapidement. Votre contact: Sandrine Ravon: agent indépendant en immobilier.");
        $ad->setPrice(172000);
        $ad->setSurface(125);

        $photo = new Photo();
        $photo->setUrl("http://5.visuels.poliris.com/bigs/5/2/1/a/521add64-3fca.jpg");

        $ad->addPhoto($photo);

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'ad' => $ad
        ]);
    }
}
