<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity()
 */
class Ad
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="url", type="string", length=250, unique=true)
     */
    private $url;

    /**
     * @ORM\Column(name="description", type="string", length=250)
     */
    private $description;

    /**
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @ORM\Column(name="surface", type="float")
     */
    private $surface;

    /**
     * @ORM\Column(name="rooms", type="integer")
     */
    private $rooms;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Photo", mappedBy="ad")
     */
    private $photos;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    /**
    * Get Id
    * @return int
    */
    public function getId()
    {
        return $this->Id;
    }

    /**
    * Get description
    * @return string
    */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
    * Set description
    * @return $this
    */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
    * Get Surface
    * @return float
    */
    public function getSurface()
    {
        return $this->Surface;
    }
    
    /**
    * Set Surface
    * @return $this
    */
    public function setSurface($Surface)
    {
        $this->Surface = $Surface;
        return $this;
    }

    /**
    * Get price
    * @return int
    */
    public function getPrice()
    {
        return $this->price;
    }
    
    /**
    * Set price
    * @return $this
    */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
    * Get rooms
    * @return int
    */
    public function getRooms()
    {
        return $this->rooms;
    }
    
    /**
    * Set rooms
    * @return $this
    */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
        return $this;
    }

    /**
    * Get url
    * @return string
    */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
    * Set url
    * @return $this
    */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Add photos
     * @return $this
     */
    public function addPhoto($photo)
    {
        $this->photos->add($photo);
        return $this;
    }
    /**
     * Remove photos
     * @return $this
     */
    public function removePhoto($photo)
    {
        $this->photos->removeElement($photo);
        return $this;
    }
    /**
     * Get photos
     * @return ArrayCollection
     */
    public function getPhotos()
    {
        return $this->photos;
    }
}
