<?php

namespace AppBundle;

use AppBundle\Parser\ParserManager;
use Doctrine\Entity\EntityManager;
use AppBundle\Entity\Ad;

class AdManager
{
    /**
     * parser Manager
     * @var ParserManager
     */
    protected $parserManager;

    /**
     * entity Manager
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct($parserManager, $entityManager)
    {
        $this->parserManager = $parserManager;
        $this->entityManager = $entityManager;
    }

    public function getAd($uri)
    {

    }

    /**
     * update Ad
     * @param  Ad $ad
     * @return Ad
     */
    private function update(Ad $ad)
    {

    }
}
