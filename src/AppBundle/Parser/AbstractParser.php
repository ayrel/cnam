<?php

namespace AppBundle\Parser;

use AppBundle\Entity\Ad;
use Symfony\Component\DomCrawler\Crawler;

abstract class AbstractParser implements ParserInterface
{
    /**
     * @var Crawler
     */
    protected $crawler;

    /**
    * Get domCrawler
    * @return Crawler
    */
    public function getCrawler()
    {
        return $this->crawler;
    }
    
    /**
    * Set domCrawler
    * @return $this
    */
    public function setCrawler(Crawler $crawler)
    {
        $this->crawler = $crawler;
        return $this;
    }

    /**
     * Récupérer le prix
     * @return int
     */
    abstract public function getPrice();
    /**
     * Récupérer la description
     * @return string
     */
    abstract public function getDescription();
    /**Get pictures
     * @return arrayCollection Photo
     */
    abstract public function getPhotoUrls();
    /**
     * Get surface
     * @return float
     */
    abstract public function getSurface();
    /**
     * Get rooms
     * @return int
     */
    abstract public function getRooms();

    /**
     * decides if the parser could parse this uri
     * @param  string $uri
     * @return bool
     */
    public function supports($uri)
    {
        $baseUrl = $this->getBaseUrl();

        if (strpos($uri, $baseUrl)===0) {
            return true;
        }

        return false;
    }

    abstract public function getBaseUrl();

    public function getAd()
    {
        $ad = new Ad();
        
        $ad->setPrice($this->getPrice());
        $ad->setDescription($this->getDescription());

        return $ad;
    }
}
