<?php

namespace AppBundle\Parser;

class ParserManager
{
    /**
     * @var Parser[]
     */
    protected $parsers;

    /**
     * Add Parser
     * @return $this
     */
    public function addParser($parser)
    {
        $this->parsers[] = $parser;
    }

    /**
     * getParser
     * @param  string $url
     * @return Parser
     */
    public function getParser($url)
    {
        foreach ($this->parsers as $parser) {
            if ($parser->supports($url)) {
                return $parser;
            }
        }

        throw new \Exception(sprintf(
            'no parser found for url : %s',
            $url
        ));
    }
}
