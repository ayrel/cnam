<?php

namespace AppBundle\Parser;

class SelogerParser extends AbstractParser
{
    protected function getTitle()
    {
        return $this->crawler->filter('title')->text();
    }

    /**
     * Récupérer le prix
     * @return int
     */
    public function getPrice()
    {
        if (preg_match("/([0-9]*)€/u", $this->getTitle(), $matches)) {
            return (int) $matches[1];
        }
        
        return null;
    }

    /**
     * Récupérer la description
     * @return string
     */
    public function getDescription()
    {
        return $this->crawler->filter("meta[name=description]")->attr('content');
    }

    /**
     * Get pictures
     * @return arrayCollection Photo
     */
    public function getPhotoUrls()
    {
        return array(
            $this->getPhotoFirstUrl()
        );
    }

    protected function getPhotoFirstUrl()
    {
        foreach ($this->crawler->filter('meta[property]') as $dom) {
            if ($dom->getAttribute('property') == "og:image") {
                return $dom->getAttribute('content');
            }
        }
    }
    
    /**
     * Get surface
     * @return float
     */
    public function getSurface()
    {
        if (preg_match("/([0-9]*)m²/u", $this->getTitle(), $matches)) {
            return (int) $matches[1];
        }
    }
    /**
     * Get rooms
     * @return int
     */
    public function getRooms()
    {
        if (preg_match("/([0-9]*) pièces|Pièces|Pièce/u", $this->getTitle(), $matches)) {
            return (int) $matches[1];
        }
        return null;
         
    }

    public function getBaseUrl()
    {
        return "http://www.seloger.com";
    }
}
