<?php

namespace AppBundle\Parser;

class LogicImmoParser extends AbstractParser
{
    public function getTitle()
    {
        return $this->crawler->filter('title')->text();
    }

    /**
     * Récupérer le prix
     * @return int
     */
    public function getPrice()
    {
        $title = $this->crawler->filter('title')->text();
        if (preg_match("/([0-9 ]*)€/u", $title, $matches)) {
            return (int) str_replace(" ", "", $matches[1]);
        }
    }

    public function getDescription()
    {
        $title = $this->crawler->filter('meta[name=description]')->attr('content');
        return html_entity_decode($title);
    }

    /**
     * Get pictures
     * Youssef Belhadj
     * @return string[]
     */
    public function getPhotoUrls()
    {
        foreach ($this->crawler->filter('meta') as $dom) {
            if ($dom->getAttribute('name') == "twitter:image:src") {
                return [ $dom->getAttribute('content') ];
            }
        }
    }

    /**
     * Get surface
     * @return float
     */
    public function getSurface()
    {
        if (preg_match("/([0-9]*)m²/u", $this->getTitle(), $matches)) {
            return (int) $matches[1];
        }
    }
    /**
     * Get rooms
     * @return int
     */
    public function getRooms()
    {
        if (preg_match("/([0-9]*) pièces/u", $this->getTitle(), $matches)) {
            return (int) $matches[1];
        }
    }

    public function getBaseUrl()
    {
        return "http://www.logic-immo.com";
    }
}
