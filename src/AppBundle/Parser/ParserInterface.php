<?php

namespace AppBundle\Parser;

interface ParserInterface
{
    /**
     *
     * @return Ad
     */
    public function getAd();
}
