<?php

namespace AppBundle\Parser;

class LeBonCoinParser extends AbstractParser
{
    public function getTitle()
    {
        return $this->crawler->filter('title')->text();
    }

    /**
     * Récupérer le prix
     * @return int
     */
    public function getPrice()
    {
        return (int) $this->crawler->filter('[itemprop=price]')->attr('content');
    }

    /**
     * Récupérer la description
     * @return string
     */
    public function getDescription()
    {
        return $this->crawler->filter("p.value[itemprop=description]")->html();
    }

    /**
     * Get pictures
     * @return arrayCollection Photo
     */
    public function getPhotoUrls()
    {
        return [
            $this->crawler->filter("img[alt]")->attr('src')
        ];
    }

    /**
     * Get surface
     * @return float
     */
    public function getSurface()
    {
        return null;
        if (preg_match("/([0-9]*)m²/u", $this->getTitle(), $matches)) {
            return (int) $matches[1];
        }
    }
    /**
     * Get rooms
     * @return int
     */
    public function getRooms()
    {
        if (preg_match("/([0-9]*) pièces/u", $this->getTitle(), $matches)) {
            return (int) $matches[1];
        }
        return null;
    }

    public function getBaseUrl()
    {
        return "http://www.leboncoin.fr";
    }
}
