<?php

namespace Tests\AppBundle\Parser;

class LogicImmoParserTest extends AbstractParser
{
    public function testGetPrice()
    {
        $this->assertEquals(
            $this->parser->getPrice(),
            210000
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->parser->getDescription(),
            "Saint etienne maison de plus de 200m2 cuisine équipée, séjour avec grande chemi...  Maison  7 pièces à vendre à Saint Etienne 42000"
        );
    }
    
    public function testGetRooms()
    {
        $this->assertEquals(
            $this->parser->getRooms(),
            7
        );
    }

    public function testGetPhotoLink()
    {
        $this->assertEquals(
            $this->parser->getPhotoUrls(),
            array(
                "http://mmf.logic-immo.com/mmf/ads/photo-prop-440x330/026/1/1b43698a-4179-4d27-a14e-5783bfcf0ad6.jpg"
            )
        );
    }

    public function getClassName()
    {
        return "\AppBundle\Parser\LogicImmoParser";
    }

    public function getFileName()
    {
        return 'lgi';
    }
}
