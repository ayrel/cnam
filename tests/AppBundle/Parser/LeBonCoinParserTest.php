<?php

namespace Tests\AppBundle\Parser;

class LeBonCoinParserTest extends AbstractParser
{
    public function testGetTitle()
    {
        $this->assertEquals(
            $this->parser->getTitle(),
            "Villa 7 pièces 140 m² Ventes immobilières Loire - leboncoin.fr"
        );
    }

    public function testGetPrice()
    {
        $this->assertEquals(
            $this->parser->getPrice(),
            295000
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->parser->GetDescription(),
            "SAINT-ETIENNE - Villa de 140 m2<br><br>SAINT-ETIENNE EST  Villa mitoyenne, de 140m² hab, de construction traditionnelle (2008), offrant une belle  une cuisine équipée, salon, séjour, 4 chambres,1 salle de bain, une spacieuse terrasse, un garage double, une piscine hors sol. système de chauffage (géothermie) sur terrain plat, clos et arboré. Proche de toutes commodités(collège, écoles, commerces et bus). DPE : C.  Contacter franck GUERIN au 06.98.65.63.45 Toutes nos annonces sur ***<br>Référence annonce : 6379"
        );
    }

    public function testGetRooms()
    {
        $this->assertEquals(
            $this->parser->getRooms(),
            7
        );
    }

    public function testsGetPhotos()
    {
        $this->assertEquals(
            $this->parser->getPhotoUrls(),
            [
                "//img7.leboncoin.fr/images/bc7/bc7fe3fbef2a4c23c01ec2e038b1196ddf66fb3b.jpg"
            ]
        );
    }

    public function getClassName()
    {
        return "\AppBundle\Parser\LeBonCoinParser";
    }

    public function getFileName()
    {
        return 'lbc';
    }
}
