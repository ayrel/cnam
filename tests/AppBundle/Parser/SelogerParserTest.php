<?php

namespace Tests\AppBundle\Parser;

class SelogerParserTest extends AbstractParser
{
    public function testGetPrice()
    {
        $this->assertEquals(
            $this->parser->getPrice(),
            172000
        );
    }

    public function testGetRooms()
    {
        $this->assertEquals(
            $this->parser->getRooms(),
            5
        );
    }

    public function testGetPhotoUrls()
    {
        $this->assertEquals(
            $this->parser->getPhotoUrls(),
            array(
                "http://static.poliris.com/z/produits/sl/assets/images/sl/sl_icon.jpg"
            )
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->parser->getDescription(),
            "MAISON DE VILLE Quartier Jean-Jaurès, 50m de la Place, au calme, fond de cour cette jolie maison de 125 m² sur 2 étages vous propose 4 chambres, 2 salles d'eau, 2 WC, un beau séjour de 25m², une grande cuisine, ces deux pièces au rez-de-chaussée avec accès au jardin de 80 m². 1 cave. Chauffage individuel au gaz, pas de travaux, en copropriété avec un petit immeuble formant 49 lots, la maison représente 351/3023 tantième des charges, Charges de 50€/mois soit 600€/an.Cette maison est actuellement louée en colocation meublée avec un bon rendement et peut être libre de tout occupant rapidement. Votre contact: Sandrine Ravon: agent indépendant en immobilier."
        );
    }

    public function testGetSurface()
    {
        $this->assertEquals(
            $this->parser->getSurface(),
            125.0
        );
    }

    public function getClassName()
    {
        return "\AppBundle\Parser\SelogerParser";
    }

    public function getFileName()
    {
        return 'slg';
    }

    public function testSupports()
    {
        $url = "http://www.seloger.com/annonces/achat/maison/saint-etienne-42/108513275.htm";
        $this->assertTrue(
            $this->parser->supports($url)
        );
    }
}
