<?php

namespace Tests\AppBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;

use AppBundle\Parser\AbstractParser as Parser;

abstract class AbstractParser extends \PHPUnit_Framework_TestCase
{
    protected $parser;

    protected function setUp()
    {
        $crawler = new Crawler(
            file_get_contents(__DIR__."/data/".$this->getFileName().".html")
        );

        $className = $this->getClassName();
        $this->parser = new $className();
        if (!$this->parser instanceof Parser) {
            throw new \Exception("the parser is not a valid parser");
        }
        $this->parser->setCrawler($crawler);
    }

    /**
     * the data file to parse
     * @return string the html content of the webpage
     */
    abstract public function getFileName();

    /**
     * AbstractParser
     * @return AbstractParser the parser to test
     */
    abstract public function getClassName();
}
